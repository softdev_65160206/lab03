/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab03;

/**
 *
 * @author informatics
 */
import java.util.Scanner;
public class Lab03 {

    public static void main(String[] args) {
        char Board[][] = {{'-','-','-'}, {'-', '-', '-'},{'-', '-', '-'}};
        
            printwelcome();
            while(true){
            printBoard(Board);
            placePiece(Board,"1");
                if (checkWin(Board).equals("X win") || checkWin(Board).equals("O win")) {
                    printBoard(Board);
                    System.out.println(checkWin(Board));
                    break;
                }
                if (checkDraw(Board)) {
                    System.out.println("Draw");
                    break;
                }
                
            printBoard(Board);
            
            placePiece(Board,"2");
             if (checkWin(Board).equals("X win") || checkWin(Board).equals("O win")) {
                    System.out.println(checkWin(Board));
                    break;
                }
                if (checkDraw(Board)) {
                    System.out.println("Draw");
                    break;
        }
            }
        
    }
    public static int add(int num1,int num2){
       return num1 + num2;
    }
    
    static String checkWin(char[][] Board){
        for(int i = 0; i < 3; i++){
        if(Board[i][0] == Board[i][1]&& Board[i][1]==Board[i][2]&&Board[i][2]!='-'){
            return Board[i][0]+" win";
        }
        else if(Board[0][i] == Board[1][i]&& Board[1][i]==Board[2][i]&&Board[2][i]!='-'){
            return Board[0][i]+" win";
        }
       
    }
        if(Board[0][0] == Board[1][1]&& Board[1][1]==Board[2][2]&&Board[2][2]!='-'){
           return Board[0][0]+" win";     
                } 
        else if(Board[0][2] == Board[1][1]&& Board[1][1]==Board[2][0]&&Board[2][0]!='-'){
           return Board[0][2]+" win";     
                }
        return"";
   
     
}

    public static boolean checkDraw(char[][] Board){
        for (int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                if(Board[i][j]== '-'){
                    return false;
                }
            }
    }
        return true;
    }
    static void placePiece(char[][] Board,String player){
        
        Scanner kb = new Scanner(System.in);
        
        char symbol = 'X';
        if (player.equals("1")){
            symbol = 'X';
        }else if(player.equals("2")){
            symbol = 'O';
        }
        System.out.println(symbol+" turn");
        System.out.print("Please input row,col:");
        String prow = kb.next();
        String pcol = kb.next();
        int prowint = Integer.parseInt(prow);
        int pcolint = Integer.parseInt(pcol);
        for (int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
        if (prowint == i && pcolint == j){
            Board[i][j] = symbol;
            break;
            }
        }
        }
        
    }
    public static void printBoard(char[][] Board){
        for(char[] row: Board){
            for(char col: row){
                System.out.print(col+" ");
            }
            System.out.println();
        }
    }
    public static void printwelcome() {
        System.out.println("Welcome to OX");
    }


}
