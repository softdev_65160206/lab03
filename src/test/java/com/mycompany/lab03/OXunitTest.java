/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab03;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class OXunitTest {

    public OXunitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    void checkWin_O_horizontal_1_Test() {
        char Board[][] = {{'O', 'O', 'O'}, {'-', '-', '-'}, {'-', '-', '-'}};
        String result = Lab03.checkWin(Board);

        assertEquals("O win", result);
    }

    @Test
    void checkWin_O_horizontal_2_Test() {
        char Board[][] = {{'-', '-', '-'}, {'O', 'O', 'O'}, {'-', '-', '-'}};
        String result = Lab03.checkWin(Board);

        assertEquals("O win", result);
    }

    @Test
    void checkWin_O_horizontal_3_Test() {
        char Board[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'O', 'O', 'O'}};
        String result = Lab03.checkWin(Board);

        assertEquals("O win", result);
    }

    @Test
    void checkWin_X_Horizontal_1_Test() {
        char Board[][] = {{'X', 'X', 'X'}, {'-', 'O', '-'}, {'-', 'O', '-'}};
        String result = Lab03.checkWin(Board);

        assertEquals("X win", result);
    }

    @Test
    void checkWin_X_Horizontal_2_Test() {
        char Board[][] = {{'-', 'O', '-'}, {'X', 'X', 'X'}, {'-', 'O', '-'}};
        String result = Lab03.checkWin(Board);

        assertEquals("X win", result);
    }

    @Test
    void checkWin_X_Horizontal_3_Test() {
        char Board[][] = {{'-', 'O', '-'}, {'-', 'O', '-'}, {'X', 'X', 'X'}};
        String result = Lab03.checkWin(Board);

        assertEquals("X win", result);
    }

    @Test
    void checkWin_O_Verical_1_Test() {
        char Board[][] = {{'O', '-', '-'}, {'O', '-', '-'}, {'O', '-', '-'}};
        String result = Lab03.checkWin(Board);

        assertEquals("O win", result);
    }

    @Test
    void checkWin_O_Verical_2_Test() {
        char Board[][] = {{'-', 'O', '-'}, {'-', 'O', '-'}, {'-', 'O', '-'}};
        String result = Lab03.checkWin(Board);

        assertEquals("O win", result);
    }

    @Test
    void checkWin_O_Verical_3_Test() {
        char Board[][] = {{'-', '-', 'O'}, {'-', '-', 'O'}, {'-', '-', 'O'}};
        String result = Lab03.checkWin(Board);

        assertEquals("O win", result);
    }

    @Test
    void checkWin_X_Verical_1_Test() {
        char Board[][] = {{'X', '-', '-'}, {'X', 'O', '-'}, {'X', 'O', '-'}};
        String result = Lab03.checkWin(Board);

        assertEquals("X win", result);
    }

    @Test
    void checkWin_X_Vertical_2_Test() {
        char Board[][] = {{'-', 'X', '-'}, {'-', 'X', 'O'}, {'-', 'X', 'O'}};
        String result = Lab03.checkWin(Board);

        assertEquals("X win", result);
    }

    @Test
    void checkWin_X_Vertical_3_Test() {
        char Board[][] = {{'-', '-', 'X'}, {'-', 'O', 'X'}, {'-', 'O', 'X'}};
        String result = Lab03.checkWin(Board);

        assertEquals("X win", result);
    }

    @Test
    void checkWin_X_Diagonal_1_Test() {
        char Board[][] = {{'X', '-', '-'}, {'-', 'X', '-'}, {'-', 'O', 'X'}};
        String result = Lab03.checkWin(Board);

        assertEquals("X win", result);
    }

    @Test
    void checkWin_X_Diagonal_2_Test() {
        char Board[][] = {{'-', '-', 'X'}, {'-', 'X', '-'}, {'X', 'O', '-'}};
        String result = Lab03.checkWin(Board);

        assertEquals("X win", result);
    }

    @Test
    void checkWin_O_Diagonal_1_Test() {
        char Board[][] = {{'O', '-', '-'}, {'-', 'O', '-'}, {'-', 'X', 'O'}};
        String result = Lab03.checkWin(Board);

        assertEquals("O win", result);
    }

    @Test
    void checkWin_O_Diagonal_2_Test() {
        char Board[][] = {{'-', '-', 'O'}, {'-', 'O', '-'}, {'O', 'O', '-'}};
        String result = Lab03.checkWin(Board);

        assertEquals("O win", result);
    }

    @Test
    void checkDraw_Success_Test() {
        char Board[][] = {{'X', 'O', 'X'},
        {'X', 'O', 'O'},
        {'O', 'X', 'X'}};
        Boolean result = Lab03.checkDraw(Board);

        assertEquals(true, result);
    }

    @Test
    void checkDraw_Fail_Test() {
        char Board[][] = {{'-', '-', '-'}, {'-', 'X', '-'}, {'-', 'X', '-'}};
        Boolean result = Lab03.checkDraw(Board);

        assertEquals(false, result); // This is expected to fail!
    }

}
